## NoPayStation List

A script to list downloadable content for PlayStation 3 from NoPayStation.com

It loads tsv files directly of NoPayStation.com
- GAMES
- DLCS
- THEMES
- AVATARS
- DEMOS

It can download for local use, and it can modify to add or change other files listed in the remote site.